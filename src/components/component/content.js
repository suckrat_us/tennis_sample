import React from 'react';

class Cont extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            match: null
        }

    }

    handleClick = (match) => {
        this.setState({match: match})
        this.props.onUpdate(this.state.match)
    }
    render() {
        return(
        <div className="task-block" onClick={this.handleClick.bind(this, this.props.match_data.match)}>
            <p>{this.props.match_data.match}</p>
        </div>
        )
    }
}

export default Cont;

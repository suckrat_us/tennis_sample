import React from 'react';


class GameSetBlock extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            homeTeamRanking: '',
            awayTeamRanking: '',
            winner:''
        }
    }
    render() {
        if(this.props.data.homeTeamRanking === undefined){
            this.setState({homeTeamRanking: "no rating"})
        }else{
            this.setState({homeTeamRanking: this.props.data.homeTeamRanking.current_rank})
        }
        if(this.props.data.awayTeamRanking === undefined){
            this.setState({awayTeamRanking: "no rating"})
        }else{
            this.setState({awayTeamRanking: this.props.data.awayTeamRanking.current_rank})
        }
        if(this.props.data.winner === 2){
            this.setState({winner: this.props.data.awayTeamName})
        }else if (this.props.data.winner === 1){
            this.setState({winner: this.props.data.homeTeamName})
        }else {
            this.setState({winner: ""})
        }
        return(

                <div className="content">
                    <div className="main-game">
                    <div className="gamer">
                        <h3>Home</h3>
                        <p>{this.props.data.homeTeamName}</p>
                        <p>Rank: {this.state.homeTeamRanking}</p>
                        <p>Score: {this.props.data.homeTeamScore}</p>
                        
                    </div>
                    <div className="gamer-two">
                        <h3>Away</h3>
                        <p>{this.props.data.awayTeamName}</p>
                        <p>Rank: {this.state.awayTeamRanking}</p>
                        <p>Score: {this.props.data.awayTeamScore}</p>
                    </div>
                    <div className="gamer-content">
                        <p>winner: {this.state.winner}</p>
                        <p>date: {this.props.data.date}</p>
                    </div>

                    </div>
                    <div className="some">
                    <GameSet data={this.props.data}/>

                    </div>
                </div>

        )
    }
}
class GameSet extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            homeTeamRanking: '',
            awayTeamRanking: '',
            winner: ''
        }
    }
    render() {
        if(this.props.data.homeTeamRanking === undefined){
            this.setState({homeTeamRanking: "no rating"})
        }else{
            this.setState({homeTeamRanking: this.props.data.homeTeamRanking.current_rank})
        }
        if(this.props.data.awayTeamRanking === undefined){
            this.setState({awayTeamRanking: "no rating"})
        }else{
            this.setState({awayTeamRanking: this.props.data.awayTeamRanking.current_rank})
        }
        if(this.props.data.winner === 2){
            this.setState({winner: this.props.data.awayTeamName})
        }else if (this.props.data.winner === 1){
            this.setState({winner: this.props.data.homeTeamName})
        }else {
            this.setState({winner: ""})
        }
        return(
            <div className="con">
                <div className="enemy-game">
                    <div className="gamer">
                        <h3>Home</h3>
                        <p>{this.props.data.lastMatch.homeTeamName}</p>
                        <p>Rank: {this.state.homeTeamRanking}</p>
                        <p>Score: {this.props.data.lastMatch.homeTeamScore}</p>
                    </div>
                    <div className="gamer-two">
                        <h3>Away</h3>
                        <p>{this.props.data.lastMatch.awayTeamName}</p>
                        <p>Rank: {this.state.awayTeamRanking}</p>
                        <p>Score: {this.props.data.lastMatch.awayTeamScore}</p>
                    </div>
                        <div className="gamer-content">
                        <p>winner: {this.state.winner}</p>
                        <p>date: {this.props.data.lastMatch.date}</p>
                        </div>
                </div>            
            </div>
        )
    }
}
export default GameSetBlock;
import React from 'react';
import DayPicker from 'react-day-picker';
import Cont from "./component/content";
import GameSetBlock from "./gamer_block/gamer_block";
import 'react-day-picker/lib/style.css';





const MATCHAPI = "http://some_api.ru/api/matches?date=";
const APIONE = "http://some_api.ru/api?date=";

function capitalizeTxt(txt) {
    return txt.charAt(0).toUpperCase() + txt.slice(1);
  }

class MainPage extends React.Component{
    constructor(props) {
        super(props);
        var to = new Date(),
          today = (to.getMonth() + 1) + '.' + to.getFullYear()+'.' ;
            if(to.getDate() <= 9){
              var day = '0'+to.getDate()
            }else{
              var day = to.getDate()
            }
            var dt= day+'.'+today;
        this.state = {
            match_data: [],
            search_data: [],
            dataone: [],
            datatwo:[],
            main_data:[],
            match_date: null,
            player1:"",
            player2:"",
            match:"",
            curent_match:null,
            selectedDay: dt,
            name: '',
            sorted_data: ""
        }
        this.searchButton = this.searchButton.bind(this);
        this.updateAll= this.updateAll.bind(this);
        this.updateMatch = this.updateMatch.bind(this);
        this.handleDayClick = this.handleDayClick.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

    }

    componentDidMount() {
        this.updateMatch();
        this.updateAll();
        this.setState({curent_match: this.state.match_data[0]})
    }
    componentWillReceiveProps(){
    }
    componentDidUpdate(nextProps){
        if(this.state.match_date !== this.state.selectedDay){
            this.setState({match_date: this.state.selectedDay})
            this.updateMatch()
        }
        if(this.state.match !== nextProps.match){
            this.setState({match: this.props.match})
            this.updateAll();
        }
    }
    onUpdate = (match) => {
        this.setState({curent_match: match})
        var match = match.split(" - ");

        this.setState({
            match:match,
            player1:match[0],
            player2:match[1],
        })
   }
    updateMatch(){
        fetch(MATCHAPI + this.state.selectedDay)
        .then(response => response.json())
        .then(match_data => this.setState({ match_data }));

        fetch(MATCHAPI + this.state.selectedDay)
        .then(response => response.json())
        .then(search_data => this.setState({ search_data }));

    }
    updateAll(){
        fetch(APIONE+this.state.match_date+"&match="+this.state.curent_match)
        .then(response => response.json())
        .then(main_data => this.setState({ main_data }));
    }
    handleDayClick(day) {
        this.state.selectedDay = day;
        if(this.state.selectedDay !== undefined){
            let vara = this.state.selectedDay.toLocaleDateString();
            let varan = vara.split(".");
            let date = varan[0]+"."+varan[1]+"."+varan[2]+".";
            this.setState({
                selectedDay:date
            })
          }
    }


    handleInputChange(event) {
        let value = capitalizeTxt(event.target.value);
        this.setState({sorted_data: value})

        let data = this.state.search_data;

        let matches = data.filter(v => v.match.includes(value));
        if (value === ''){
            this.updateMatch();
        }
        this.setState({
            search_data: matches,
        });
      }
      searchButton(){
        let value = this.state.sorted_data;
        let data = this.state.match_data;
        let matches = data.filter(v => v.match.includes(value));
        this.setState({
            search_data: matches,
        });
      }
      
    render() {
        if(this.state.curent_match === null){
            this.setState({curent_match: this.state.match_data[0]})
        }
        if(this.state.match_data === []){
            console.log("null")
        }

        return(
            <div>
                <div className="calendar-bar">
                <div className="calendar">
                    <DayPicker
                        onDayClick={this.handleDayClick}
                        selectedDay={this.state.selectedDay}
                    /> 
                </div>
 
                    <div className="game-table"> 
                        <h3>Games on {this.state.match_date}</h3>
                        <div className="search">
                        <p>Search:</p>
                             <input className="text-field" type="text" name="name" onChange={this.handleInputChange} />
                             <button onClick={this.searchButton}>search</button>
                        </div>
                        <div className="game-table-content">
                            {this.state.search_data.map((search_data,i) => <Cont key={i} match_data={search_data} onUpdate={this.onUpdate}> </Cont>)}
                        </div>
                    </div>
                </div>
                <ContentWindow main_data={this.state.main_data}> </ContentWindow>
            </div>

        )
    }
}


class ContentWindow extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            playerHome: "Not Selected",
            playerAway: "Not Selected",
            homeScore:"no score",
            awayScore:"no score",
            data:[],
            dataone: [],
            datatwo: []

        }
    }
    render() {
        if (this.props.main_data.length > 0) {
            this.setState({homeScore: this.props.main_data[0].homeTeamScore}),
            this.setState({awayScore: this.props.main_data[0].awayTeamScore}),
            this.setState({playerHome: this.props.main_data[0].homeTeamName}),
            this.setState({playerAway: this.props.main_data[0].awayTeamName}),
            this.setState({dataone: this.props.main_data[0].homeTeamMatches}),
            this.setState({datatwo: this.props.main_data[0].awayTeamMatches})
          }
        return(
            <div className="gamer-block">
            <div className="gamer-block-content">
                <div className="gamer-name">
                    <h1>{this.state.playerHome}</h1>
                    <p>{this.state.homeScore}</p>
                </div>
                {this.state.dataone.map((dataone,key) => <GameSetBlock key={key} data={dataone}/> )}
            </div>
            <div className="gamer-block-content">
                <div className="gamer-name">
                    <h1>{this.state.playerAway}</h1>
                    <p>{this.state.awayScore}</p>
                </div>
                {this.state.datatwo.map((datatwo,key) => <GameSetBlock key={key} data={datatwo}/> )}
            </div>                             
                               
            </div> 
        )
    }
}


export default MainPage;